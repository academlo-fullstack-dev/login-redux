const INITIAL_STATE = {
    user: false
}


const authReducer = (prevState = INITIAL_STATE, action) => {

    switch(action.type){
        case "SET_USER":
            return {...prevState, user: action.userObj};
        case "REGISTER":
            break;
        case "CATCH_VALUE":
            break;
        case "LOGOUT":
            break;
        case "CHECK_ACTIVE_SESSION":
            break;
        default:
            return prevState;
    }

}

export default authReducer;