import firebase from "../firebase";

//1. login
//2. register
//3. catchValue
//4. logout
//5. checkActiveSession

//Actions Creators
const setUser = (userObj) => { //Una vez que el usuario inicie sesión guardarmos los datos del usuario en el store
    return {
        type: "SET_USER",
        userObj
    }
}

export const login = (provider, email, password) => {
    return (dispatch, state) => {
        return new Promise(async(resolve, reject) => {
            try{
                if(provider === "email"){
                    let {user} = await firebase.auth().signInWithEmailAndPassword(email, password);
                    dispatch(setUser(user));
                }else{
                    let googleProvider = new firebase.auth.GoogleAuthProvider();
                    let {user} = await firebase.auth().signInWithPopup(googleProvider);
                    dispatch(setUser(user));                    
                }
                resolve(true);
            }catch(error){
                console.log(error);
                reject(error);
            }
        });
    }
}

export const register = (name, email, password) => {
    let defaultPhotoUrl = "https://www.gravatar.com/avatar/00000000000000000000000000000000?d=robohash";
    return async(dispatch, state) => {
        try{
            let userObj = await firebase.auth().createUserWithEmailAndPassword(email, password);
            let user = firebase.auth().currentUser;
            await user.updateProfile({displayName: name, photoURL: defaultPhotoUrl});
            console.log(userObj);          
        }catch(error){
            console.log(error);            
        }
    }
}

export const logout = () => {
    return {
        type: "LOGOUT"
    }
}

export const checkActiveSession = (user) => {
    return {
        type: "CHECK_ACTIVE_SESSION",
        user
    }
}