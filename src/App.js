import LoginContainer from "./containers/LoginContainer";
import RegisterContainer from "./containers/RegisterContainer";
import ChatContainer from "./containers/ChatContainer";
import "./App.css";
import { Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/" exact>
          <LoginContainer />
        </Route>
        <Route path="/registro">
          <RegisterContainer />
        </Route>
        <Route path="/chat">
          <ChatContainer />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
